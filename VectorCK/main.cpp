#include <iostream>
#include <sstream>
#include <vector>

class vector3D
{

private:
    double x;
    double y;
    double z;

public:
    vector3D()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    double getX() { return x; }
    double getY() { return y; }
    double getZ() { return z; }

    void setXYZ(double xSet, double ySet, double zSet)
    {
        x = xSet;
        y = ySet;
        z = zSet;
    }

    void setX(double xSet) { x = xSet; }
    void setY(double ySet) { y = ySet; }
    void setZ(double zSet) { z = zSet; }

    vector3D addVectors(vector3D vector2)
    {
        vector3D returnVector;
        returnVector.x=this->x + vector2.x;
        returnVector.y=this->y + vector2.y;
        returnVector.z=this->z + vector2.z;

        return returnVector;
    }

    double dotProduct(vector3D vector1, vector3D vector2)
    {
        return vector1.x * vector2.x +
               vector1.y * vector2.y +
               vector1.z * vector2.z;
    }

    std::vector<double> crossProduct(vector3D vector1)
    {
        std::vector<double> returnVector;

        if(vector1.x == 0 && vector1.y == 0 && vector1.z == 0)
            throw "Cross product with empty vector";

        returnVector.push_back(this->y * vector1.z -
                               this->z * vector1.y);
        returnVector.push_back(this->x * vector1.z -
                               this->z * vector1.x);
        returnVector.push_back(this->x * vector1.y -
                               this->y * vector1.x);
        return returnVector;
    }

    std::string getXYZ() { return "(" + std::to_string(x) + "," + std::to_string(y) + "," + std::to_string(z) + ")"; }

    void printXYZ() { std::cout <<  "(" + std::to_string(x) + "," + std::to_string(y) + "," + std::to_string(z) + ")"; }

    ~vector3D() { std::cout << "Wektor being deleted" << std::endl;}

};

int main()
{

    vector3D wektorSpr1, wektorSpr2;
    std::cout << wektorSpr1.getX() << std::endl;

    wektorSpr1.setX(5);
    std::cout << wektorSpr1.getX() << std::endl;

    wektorSpr1.setX(4);
    wektorSpr1.setY(9);
    wektorSpr1.setZ(3);

    wektorSpr2.setX(0);
    wektorSpr2.setY(0);
    wektorSpr2.setZ(3);

    vector3D wektorSum = wektorSpr1.addVectors(wektorSpr2);
    std::cout << wektorSum.getXYZ() << std::endl;
    wektorSum.printXYZ();
    std::cout << std::endl;

    double dotProduct = wektorSpr1.dotProduct(wektorSpr1, wektorSpr2);
    std::cout << dotProduct << std::endl;

    try
    {
        std::vector<double> crossProduct;
        crossProduct.resize(3);
        crossProduct = wektorSpr1.crossProduct(wektorSpr2);
        std::cout << crossProduct[0] << " " << crossProduct[1] << " " << crossProduct[2] << std::endl;
    }
    catch (const char* msg)
    {
        std::cout << msg << std::endl;
    }


    return 0;
}
